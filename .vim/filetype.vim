if exists("did_load_filetypes")
    finish
endif
augroup filetypedetect
    au BufNewFile,BufRead *.x setf alex
    au BufNewFile,BufRead *.y setf happy
    au BufNewFile,BufRead *.json set filetype=json foldmethod=syntax
augroup END
