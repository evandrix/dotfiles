#!/bin/bash

# Test whether your terminal supports the following ansi escape sequences
for i in {1..50}; do
    echo $i  " " "\033["$i"mfoobarblahbizbang\033[0m";
done
