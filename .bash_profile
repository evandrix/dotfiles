# necessary b/c google laptop has some custom bashrc settings. my settings are
# in .kevin_bashrc
BASHRCS="
$HOME/.bashrc
$HOME/.kevin_bashrc"

for bashrc in $BASHRCS
do
    if [ -e $bashrc ]; then
        source $bashrc
    fi
done

# set up path
export PATH="/usr/local/bin":$PATH

PATHDIRS="
/usr/local/texlive/2010/bin/universal-darwin
/cs/ACM
$HOME/bin/autojump/bin
$HOME/.cabal/bin
/usr/local/bin/logging_vim/bin
/usr/local/mysql/bin
/usr/local/share/python
/usr/local/scala/scala-2.8.0.final/bin
/opt/local/Library/Frameworks/Python.framework/Versions/2.6/bin
/opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin
/opt/local/etc
/opt/local/bin
/opt/local/sbin
/Volumes/Xshare/kburke/bin
/usr/local/Cellar/ruby/1.9.2-p290/bin
$HOME/.gem/ruby/1.8/bin
$HOME/bin"
for dir in $PATHDIRS
do
    if [ -d $dir ]; then
        #export PATH=$PATH:$dir
        export PATH=$dir:$PATH
    fi
done

export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad


PORTMANPATH="/opt/local/share/man"
if [ -d $PORTMANPATH ]; then
    export MANPATH=$PORTMANPATH:$MANPATH
fi

if [ -x /opt/local/bin/vmail ];
then
    export VMAIL_VIM=mvim
fi

export HISTSIZE=10000

# ignore some files for tab completion
export FIGNORE=$FIGNORE:.hi:.pyc:.o:.beam:.dSYM:.un~

alias less='less -M'
alias ll='ls -lah'
alias vi=vim
alias svi='sudo vim'
alias mv='mv -i'
alias cp='cp -iv'
alias grep='grep -i --color=auto'
alias jx='javac \!$.java ; java \!$'
alias viewmemory="ps -u $USER -o rss,command | grep -v peruser | awk '{sum+=\$1} END {print sum/1024}'"
alias viewprocesses="ps -u $USER -o rss,etime,pid,command"

# these save so much time
alias ..="cd .."
alias ..2="cd ../.."
alias ..3="cd ../../.."
alias ..4="cd ../../../.."
alias ..5="cd ../../../../.."
alias ..6="cd ../../../../../.."
alias ..7="cd ../../../../../../.."

# linux
hash gvim 2>&- && {
    alias vi=gvim;
    alias svi='sudo gvim'
}

# set up macvim, if it exists
hash mvim 2>&- && { 
    alias vi=mvim; 
    alias svi='sudo mvim'
}

# ask for conf if deleting more than three files; otherwise, delete w/o conf
# use rm -I if it exists
hash grm 2>&- && {
    alias rm='grm -I';
}

if [ "$HOME" = '/Users/kevin' ]; then
    alias chrome='open /Applications/Google\ Chrome.app --args -omnibox-popup-count=10 --enable-match-preview'
    alias copen='open -a "Google Chrome"'
fi

if [ -f /opt/local/etc/bash_completion ]; then
      . /opt/local/etc/bash_completion
fi

hash brew 2>&- && { 
    if [ -f `brew --prefix`/etc/bash_completion ]; then
        . `brew --prefix`/etc/bash_completion
    fi
}

SCREENSET=""
if [ "$TERM" == "screen" ];
then
    SCREENSET="[screen]\n"
fi

## set editor preference
export EDITOR=vi
export VISUAL=vi
#export PAGER=less


## node.js settings
#OPT_LOCAL_LIB_PATH="/opt/local/lib"
#if [ -d $OPT_LOCAL_LIB_PATH ]; then
    #export NODE_PATH=$OPT_LOCAL_LIB_PATH:$NODE_PATH
#fi

## virtualenvwrapper
#if [ -f /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin/virtualenvwrapper.sh ]; then
    #source /opt/local/Library/Frameworks/Python.framework/Versions/2.7/bin/virtualenvwrapper.sh
#fi

##prev. prompt command has jumpstat stuff - append on red colored directory
export PROMPT_COMMAND='DIR=`pwd|sed -e "s!$HOME!~!"`; if [ ${#DIR} -gt 60 ]; then CurDir=${DIR:0:27}...${DIR:${#DIR}-30}; else CurDir=$DIR; fi ; '${PROMPT_COMMAND:-:}

# custom terminal colors
function EXT_COLOR () { 
    echo -ne "[38;5;$1m"; 
}
DEFAULT="[37m"
BLUE="`EXT_COLOR 79`"
ORANGE="`EXT_COLOR 172`"
YELLOW="`EXT_COLOR 226`"
GREEN="`EXT_COLOR 82`"

# XXX if we're on an unknown machine
PROMPT_COLOR="`EXT_COLOR 55`"
#production server, want red arrow
# XXX should prob change this to match any Linux box
if [ "$HOME" = '/home/kevinburke' ]; then
    alias ls='ls --color';
    PROMPT_COLOR="`EXT_COLOR 196`"
fi

## set my prompt
## need to wrap the colors in \[ and \] so they're not counted.
## see for example 
## http://stackoverflow.com/questions/3567743/dynamic-elements-in-bash-ps1
PS1="\n${SCREENSET}\[\e${ORANGE}\]\u \[\e${DEFAULT}\]at \[\e${BLUE}\]\h \[\e${DEFAULT}\]in \[\e${YELLOW}\]\$CurDir\n\$(__git_ps1 '\[\e${GREEN}\](%s) ')\[\e${PROMPT_COLOR}\]\#> \[\e[m"

function fuck () { echo "Calm down"; }
function FUCK () { echo "Calm down"; }
function FUCKFUCK () { echo "Calm down"; }

# temp heroku settings
export DATABASE_URL=postgresql://localhost:5432/smsauth

echo -e "\n"
date

# clojure
export CLASSPATH=$CLASSPATH:/usr/local/Cellar/clojure-contrib/1.2.0/clojure-contrib.jar

export WORKON_HOME=~/.envs
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
source /usr/local/share/python/virtualenvwrapper.sh


[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
